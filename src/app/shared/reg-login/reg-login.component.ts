import { Component, OnInit } from '@angular/core';
import {
    FormGroup,
    FormControl,
    Validators,
    FormBuilder,
    NgForm
} from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
declare var jquery: any;
declare var $: any;

import { PublisherService, PublishData } from '../../services/publisher.service';
import { AuthenticationService } from '../../services/authentication/authentication.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'guesspage-reg-login',
  templateUrl: './reg-login.component.html',
  styleUrls: ['./reg-login.component.css'],
})
export class RegLoginComponent implements OnInit {
  public loginForm: FormGroup;
  public email: FormControl;
  public password: FormControl;

  public registerData: any = {usertype : 2};

  public toggleForm = true;

  public loginError: any = {
    display: false,
    message: ''
  };
  public signUpError: any = {
    display: false,
    message: ''
  };
  public signUpSuccess: any = {
    display: false,
    message: ''
  };

  constructor(
    private auth: AuthenticationService,
    private router: Router,
    private _publisher: PublisherService
  ) { }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
    // Event published from header component (toggling signup/login form)
    this._publisher.on<PublishData>('header.modal.click')
    .subscribe(data => {
        this.toggleForm = data.message;
    });
  }

  createFormControls() {
    this.email = new FormControl('');
    this.password = new FormControl('');
  }

  createForm() {
    this.loginForm = new FormGroup({
      email: this.email,
      password: this.password
    });
  }

  toggleFormFunction() {
    this.toggleForm = !this.toggleForm;
  }

  onLogin() {
    this.auth.authenticate({'email': this.email.value, 'password': this.password.value})
    .subscribe(result => {
      if (result.status === 'OK') {
        $('#myModal-signup').modal('hide');
        this.router.navigate(['/positions']);
      } else {
        this.loginError.display = true;
        this.loginError.message = result.message;
      }
    },
    error => {
      this.loginError.display = true;
      this.loginError.message = error.message;
    });
  }

  /**
   *
   * @param type
   */
  getSugnUpType(type) {
    // tslint:disable-next-line:radix
    this.registerData.usertype = parseInt(type);
  }

  /**
   *
   */
  onSignUp() {
    if (!this.registerData.username || !this.registerData.email || !this.registerData.password || !this.registerData.usertype) {
      this.signUpError.display = true;
      this.signUpError.message = 'All fields are mandatory.';
    } else {
      this.signUpError.display = false;
      this.auth.register(this.registerData)
      .subscribe(result => {
        this.signUpSuccess.display = true;
        this.signUpSuccess.message = 'You have registered successfully.Please check your email to activate your account';
      },
      error => {
        this.signUpError.display = true;
        this.signUpError.message = error.message;
      });
    }
  }
}
