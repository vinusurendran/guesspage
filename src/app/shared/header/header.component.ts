import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PublisherService } from '../../services/publisher.service';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { SessionService } from '../../services/session/session.service';
// import { ActiveState } from "../../services/shared/ActiveState.service";

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'guesspage-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  public whichHeader = null;
  public currentStateName = null;
  public isSession = false;
  /**
   * Constructor
   * @param route
   */
  constructor(
    private _routeActivate: ActivatedRoute,
    private _route: Router,
    private _publisher: PublisherService,
    private _auth: AuthenticationService,
    private _session: SessionService
  ) {
    if (this._routeActivate.snapshot.url[0].path === 'home') {
      this.whichHeader = 'homeHeader';
    } else {
      this.whichHeader = 'loginHeader';
    } // end if else
    this.currentStateName = this._routeActivate.snapshot.url[0].path;
    this.isSession = this._session.isLoggedIn();
  }

  ngOnInit() { }

  ngAfterViewInint() { }

  showLoginOrSignup(flag) {
    // event published and subscribed in reg-login component
    this._publisher.broadcast('header.modal.click', { message: flag });
  }

  logout() {
    this._auth.logout();
    this._route.navigate(['/home']);
  }
}
