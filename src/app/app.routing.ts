import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';

import { LoginActivate } from './services/authentication/login-activate.service';

import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { PositionsComponent } from './positions/positions.component';
import { EmployerComponent } from './employer/employer.component';
import { AgencyComponent } from './agency/agency.component';

export const AppRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'user', component: UserComponent },
    { path: 'positions', component: PositionsComponent, canActivate: [LoginActivate] },
    { path: 'employer', component: EmployerComponent, canActivate: [LoginActivate] },
    { path: 'agency', component: AgencyComponent, canActivate: [LoginActivate] }
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
