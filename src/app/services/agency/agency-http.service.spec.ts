import { TestBed, inject } from '@angular/core/testing';

import { AgencyHttpService } from './agency-http.service';

describe('AgencyHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgencyHttpService]
    });
  });

  it('should be created', inject([AgencyHttpService], (service: AgencyHttpService) => {
    expect(service).toBeTruthy();
  }));
});
