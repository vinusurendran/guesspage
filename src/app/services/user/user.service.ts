import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { UserHttpService } from './user-http.service';
import { SessionService } from '../session/session.service';
@Injectable()
export class UserService {

  constructor(
    private userHttp: UserHttpService,
    private session: SessionService
  ) { }

  /**
   *
   */
  getProfile() {
    return Observable.create(observer => {
      try {
        const data = {
          'accessToken': this.session.getToken(),
          'userid' : this.session.getUserId()
        };
        this.userHttp.getProfile(data)
        .subscribe(result => {
          if (result.status === 'OK') {
            observer.next({status: 'OK', data: result.data});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'Error in fetching profile details'});
            observer.complete();
          }
        },
        error => {
          console.log(error);
          observer.error({status: 'ERROR', message: 'Something went wrong.'});
          observer.complete();
        });
      } catch (error) {
        console.log(error);
        observer.error({status: 'ERROR', message: 'Something went wrong.'});
        observer.complete();
      }
    });
  }

  /**
   *
   */
  updateProfile(data) {
    return Observable.create(observer => {
      try {
        data.accessToken = this.session.getToken();
        data.userid = this.session.getUserId();
        this.userHttp.updateProfile(data)
        .subscribe(result => {
          if (result.status === 'OK') {
            observer.next({status: 'OK', data: result.message});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'Error in updating profile details'});
            observer.complete();
          }
        },
        error => {
          console.log(error);
          observer.error({status: 'ERROR', message: 'Something went wrong.'});
          observer.complete();
        });
      } catch (error) {
        console.log(error);
        observer.error({status: 'ERROR', message: 'Something went wrong.'});
        observer.complete();
      }
    });
  }

  /**
   *
   */
  getCandidates() {
    return Observable.create(observer => {
      try {
        const data = {
          'accessToken': this.session.getToken(),
          'agencyid' : this.session.getUserId()
        };
        this.userHttp.getCandidates(data)
        .subscribe(result => {
          if (result.status === 'OK') {
            observer.next({status: 'OK', data: result.data});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: result['message']});
            observer.complete();
          }
        },
        error => {
          console.log(error);
          observer.error({status: 'ERROR', message: 'Something went wrong.'});
          observer.complete();
        });
      } catch (error) {
        console.log(error);
        observer.error({status: 'ERROR', message: 'Something went wrong.'});
        observer.complete();
      }
    });
  }

  addCandidate(data) {
    return Observable.create(observer => {
      try {
        data.accessToken = this.session.getToken();
        data.userid = this.session.getUserId();
        data.currency_id = 53;
        data.emp_type = 'IT';
        data.choice_skills = 'development';
        data.currency_symbol = 'र';
        data.bonus = '0';
        this.userHttp.addCandidate(data)
        .subscribe(result => {
          if (result.status === 'OK') {
            observer.next({status: 'OK', data: result.message});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'Error in adding candidate'});
            observer.complete();
          }
        },
        error => {
          console.log(error);
          observer.error({status: 'ERROR', message: 'Something went wrong.'});
          observer.complete();
        });
      } catch (error) {
        console.log(error);
        observer.error({status: 'ERROR', message: 'Something went wrong.'});
        observer.complete();
      }
    });
  }
}
