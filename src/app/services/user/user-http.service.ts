import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppSettings } from '../../app.settings';

@Injectable()
export class UserHttpService {

  constructor(
    private http: HttpClient
  ) { }

  getProfile(data) {
    return Observable.create(observer => {
      try {
        this.http.post(AppSettings.API_URL + 'viewProfile/', data)
        .subscribe(result => {
          if (result['status'] === 200) {
            observer.next({status: 'OK', data: result['details'][0]});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'registration http response error', details: result});
            observer.complete();
          }
        },
        error => {
          observer.error({status: 'ERROR', message: 'registration http requset error', details: error});
          observer.complete();
        });
      } catch (error) {
        observer.error({status: 'ERROR', message: 'registration syntax error', details: error});
        observer.complete();
      }
    });
  }

  updateProfile(data) {
    return Observable.create(observer => {
      try {
        this.http.put(AppSettings.API_URL + 'editprofile/', data)
        .subscribe(result => {
          if (result['status'] === 200) {
            observer.next({status: 'OK', data: result['message']});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'registration http response error', details: result});
            observer.complete();
          }
        },
        error => {
          observer.error({status: 'ERROR', message: 'registration http requset error', details: error});
          observer.complete();
        });
      } catch (error) {
        observer.error({status: 'ERROR', message: 'registration syntax error', details: error});
        observer.complete();
      }
    });
  }

  getCandidates(data) {
    return Observable.create(observer => {
      try {
        this.http.post(AppSettings.API_URL + 'listagencyCandidates/', data)
        .subscribe(result => {
          if (result['status'] === 200) {
            observer.next({status: 'OK', data: result['details']});
            observer.complete();
          } else if (result['status'] === 204) {
            observer.next({status: 'ERROR', message: result['message'], details: result});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'listagencyCandidates http response error', details: result});
            observer.complete();
          }
        },
        error => {
          observer.error({status: 'ERROR', message: 'listagencyCandidates http requset error', details: error});
          observer.complete();
        });
      } catch (error) {
        observer.error({status: 'ERROR', message: 'listagencyCandidates syntax error', details: error});
        observer.complete();
      }
    });
  }

  addCandidate(data) {
    return Observable.create(observer => {
      try {
        // observer.next({status: "OK", data: {}})
        // observer.complete();
        this.http.post(AppSettings.API_URL + 'addCandidate/', data)
        .subscribe(result => {
          if (result['status'] === 201) {
            observer.next({status: 'OK', data: result['message']});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'candidate add http response error', details: result});
            observer.complete();
          }
        },
        error => {
          observer.error({status: 'ERROR', message: 'candidate add http requset error', details: error});
          observer.complete();
        });
      } catch (error) {
        observer.error({status: 'ERROR', message: 'candidate add syntax error', details: error});
        observer.complete();
      }
    });
  }

}
