import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  constructor() { }

  set(key, value) {
    localStorage.setItem(key, value);
  }

  get(key) {
    return localStorage.getItem(key);
  }

  remove(key) {
    localStorage.remove(key);
  }

  flush() {
    localStorage.clear();
  }
}
