import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

export interface PublishEvent {
  key: any;
  data: PublishData;
}

export interface PublishData {
  message: any;
}

export class PublisherService {
  private _eventBus: Subject<PublishEvent>;
  public showAlert = false;
  constructor() {
    this._eventBus = new Subject<PublishEvent>();
  }
   /**
    * @decsripion:  class to broadcast
    * @param key
    * @param data object
    */
  broadcast(key: any, data: any) {
    this._eventBus.next({key, data});
  }
  /**
    * @decsripion:  to filter based on key
    * @param key
    */
  on<T>(key: any): Observable<T> {
    return this._eventBus.asObservable()
      .filter(event => event.key === key)
      .map(event => <any>event.data);
  }
}
