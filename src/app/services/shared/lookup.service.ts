import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { LookupHttpService } from './lookup-http.service';
import { SessionService } from '../session/session.service';


@Injectable()
export class LookupService {

  constructor(
    private lookupHttp: LookupHttpService,
    private session: SessionService
  ) {  }

  /**
   *
   */
  getCountryList() {
    return Observable.create(observer => {
      try {
        const data = {
          'accessToken': this.session.getToken(),
          'userid' : this.session.getUserId()
        };
        this.lookupHttp.getCountryList(data)
        .subscribe(result => {
          if (result.status === 'OK') {
            observer.next({status: 'OK', data: result.data});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'Error in fetching country list'});
            observer.complete();
          }
        },
        error => {
          console.log(error);
          observer.error({status: 'ERROR', message: 'Something went wrong.'});
          observer.complete();
        });
      } catch (error) {
        console.log(error);
        observer.error({status: 'ERROR', message: 'Something went wrong.'});
        observer.complete();
      }
    });
  }

  /**
   * 
   */
  getStateList(countryId) {
    return Observable.create(observer => {
      try {
        const data = {
          'accessToken': this.session.getToken(),
          'userid' : this.session.getUserId(),
          'countryid': countryId
        };
        this.lookupHttp.getStateList(data)
        .subscribe(result => {
          if (result.status === 'OK') {
            observer.next({status: 'OK', data: result.data});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'Error in fetching state list'});
            observer.complete();
          }
        },
        error => {
          console.log(error);
          observer.error({status: 'ERROR', message: 'Something went wrong.'});
          observer.complete();
        });
      } catch (error) {
        console.log(error);
        observer.error({status: 'ERROR', message: 'Something went wrong.'});
        observer.complete();
      }
    });
  }

  /**
   * 
   */
  getLocationList(stateId) {
    return Observable.create(observer => {
      try {
        const data = {
          'accessToken': this.session.getToken(),
          'userid' : this.session.getUserId(),
          'provinceid': stateId
        };
        this.lookupHttp.getLocationList(data)
        .subscribe(result => {
          if (result.status === 'OK') {
            observer.next({status: 'OK', data: result.data});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'Error in fetching location list'});
            observer.complete();
          }
        },
        error => {
          console.log(error);
          observer.error({status: 'ERROR', message: 'Something went wrong.'});
          observer.complete();
        });
      } catch (error) {
        console.log(error);
        observer.error({status: 'ERROR', message: 'Something went wrong.'});
        observer.complete();
      }
    });
  }

}
