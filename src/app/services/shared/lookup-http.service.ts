import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppSettings } from '../../app.settings';


@Injectable()
export class LookupHttpService {

  constructor(
    private http: HttpClient
  ) {  }

  getCountryList(data) {
    return Observable.create(observer => {
      try {
        let result = {"status":"201","success":true,"Country Details":[{"country_id":"26","country_code":"AF","country_name":"Afghanistan"},{"country_id":"28","country_code":"AL","country_name":"Albania"},{"country_id":"29","country_code":"DZ","country_name":"Algeria"},{"country_id":"30","country_code":"AR","country_name":"Argentina"},{"country_id":"27","country_code":"AU","country_name":"Australia"},{"country_id":"31","country_code":"AT","country_name":"Austria"},{"country_id":"3","country_code":"BHN","country_name":"Bahrain"},{"country_id":"14","country_code":"BGD","country_name":"Bangladesh"},{"country_id":"32","country_code":"BB","country_name":"Barbados"},{"country_id":"33","country_code":"BE","country_name":"Belgium"},{"country_id":"34","country_code":"BT","country_name":"Bhutan"},{"country_id":"35","country_code":"BR","country_name":"Brazil"},{"country_id":"36","country_code":"KH","country_name":"Cambodia"},{"country_id":"37","country_code":"CM","country_name":"Cameroon"},{"country_id":"38","country_code":"CA","country_name":"Canada"},{"country_id":"21","country_code":"CHN","country_name":"China"},{"country_id":"39","country_code":"CZ","country_name":"Czech Republic"},{"country_id":"40","country_code":"DK","country_name":"Denmark"},{"country_id":"41","country_code":"EG","country_name":"Egypt"},{"country_id":"42","country_code":"ET","country_name":"Ethiopia"},{"country_id":"43","country_code":"FJ","country_name":"Fiji"},{"country_id":"44","country_code":"FI","country_name":"Finland"},{"country_id":"45","country_code":"FR","country_name":"France"},{"country_id":"20","country_code":"DEU","country_name":"Germany"},{"country_id":"47","country_code":"GH","country_name":"Ghana"},{"country_id":"48","country_code":"GR","country_name":"Greece"},{"country_id":"49","country_code":"GL","country_name":"Greenland"},{"country_id":"50","country_code":"HT","country_name":"Haiti"},{"country_id":"51","country_code":"HK","country_name":"Hong Kong"},{"country_id":"52","country_code":"HU","country_name":"Hungary"},{"country_id":"12","country_code":"IND","country_name":"India"},{"country_id":"54","country_code":"ID","country_name":"Indonesia"},{"country_id":"4","country_code":"IRN","country_name":"Iran"},{"country_id":"5","country_code":"IRQ","country_name":"Iraq"},{"country_id":"55","country_code":"IE","country_name":"Ireland"},{"country_id":"56","country_code":"IL","country_name":"Israel"},{"country_id":"53","country_code":"IT","country_name":"Italy"},{"country_id":"25","country_code":"CIV","country_name":"Ivory Coast"},{"country_id":"57","country_code":"JM","country_name":"Jamaica"},{"country_id":"58","country_code":"JP","country_name":"Japan"},{"country_id":"10","country_code":"JDN","country_name":"Jordan"},{"country_id":"59","country_code":"KZ","country_name":"Kazakhstan"},{"country_id":"60","country_code":"KE","country_name":"Kenya"},{"country_id":"6","country_code":"KWT","country_name":"Kuwait"},{"country_id":"63","country_code":"LA","country_name":"Laos"},{"country_id":"64","country_code":"LV","country_name":"Latvia"},{"country_id":"16","country_code":"LB","country_name":"Lebanon"},{"country_id":"65","country_code":"LS","country_name":"Lesotho"},{"country_id":"66","country_code":"LR","country_name":"Liberia"},{"country_id":"15","country_code":"LBY","country_name":"Libya"},{"country_id":"67","country_code":"MO","country_name":"Macau"},{"country_id":"68","country_code":"MK","country_name":"Macedonia"},{"country_id":"69","country_code":"MG","country_name":"Madagascar"},{"country_id":"70","country_code":"MW","country_name":"Malawi"},{"country_id":"71","country_code":"MY","country_name":"Malaysia"},{"country_id":"23","country_code":"MV","country_name":"Maldives"},{"country_id":"72","country_code":"ML","country_name":"Mali"},{"country_id":"73","country_code":"MM","country_name":"Myanmar"},{"country_id":"74","country_code":"NA","country_name":"Namibia"},{"country_id":"75","country_code":"NP","country_name":"Nepal"},{"country_id":"76","country_code":"NL","country_name":"Netherlands"},{"country_id":"61","country_code":"KP","country_name":"North-Korea"},{"country_id":"7","country_code":"QAR","country_name":"Qatar"},{"country_id":"8","country_code":"KSA","country_name":"Saudi Arabia"},{"country_id":"18","country_code":"Singapore","country_name":"SGP"},{"country_id":"19","country_code":"SGP","country_name":"Singapore"},{"country_id":"24","country_code":"KR","country_name":"South Korea"},{"country_id":"62","country_code":"KR","country_name":"South-Korea"},{"country_id":"17","country_code":"LK","country_name":"Sri Lanka"},{"country_id":"13","country_code":"SDN","country_name":"Sudan"},{"country_id":"2","country_code":"OMN","country_name":"Sultanate Of Oman"},{"country_id":"1","country_code":"UAE","country_name":"United Arab Emirates"},{"country_id":"11","country_code":"UK","country_name":"United Kingdom"},{"country_id":"22","country_code":"WA","country_name":"West Africa"},{"country_id":"9","country_code":"YMN","country_name":"Yemen"}]};
        if (result['status'] === "201") {
          observer.next({status: 'OK', data: result['Country Details']});
          observer.complete();
        }
        // this.http.post(AppSettings.API_URL + 'listCountries/', data)
        // .subscribe(result => {
        //   if (result['status'] === "201") {
        //     observer.next({status: 'OK', data: result['Country Details']});
        //     observer.complete();
        //   } else {
        //     observer.error({status: 'ERROR', message: 'listCountries http response error', details: result});
        //     observer.complete();
        //   }
        // },
        // error => {
        //   observer.error({status: 'ERROR', message: 'listCountries http requset error', details: error});
        //   observer.complete();
        // });
      } catch (error) {
        observer.error({status: 'ERROR', message: 'listCountries syntax error', details: error});
        observer.complete();
      }
    });
  }

  /**
   * 
   */
  getStateList(data) {
    return Observable.create(observer => {
      try {
        this.http.post(AppSettings.API_URL + 'listProvinces/', data)
        .subscribe(result => {
          if (result['status'] === "201") {
            observer.next({status: 'OK', data: result['State Details']});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'listProvinces http response error', details: result});
            observer.complete();
          }
        },
        error => {
          observer.error({status: 'ERROR', message: 'listProvinces http requset error', details: error});
          observer.complete();
        });
      } catch (error) {
        observer.error({status: 'ERROR', message: 'listProvinces syntax error', details: error});
        observer.complete();
      }
    });
  }

  /**
   * 
   */
  getLocationList(data) {
    return Observable.create(observer => {
      try {
        this.http.post(AppSettings.API_URL + 'listLocations/', data)
        .subscribe(result => {
          if (result['status'] === "201") {
            observer.next({status: 'OK', data: result['Location Details']});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: 'listLocations http response error', details: result});
            observer.complete();
          }
        },
        error => {
          observer.error({status: 'ERROR', message: 'listLocations http requset error', details: error});
          observer.complete();
        });
      } catch (error) {
        observer.error({status: 'ERROR', message: 'listLocations syntax error', details: error});
        observer.complete();
      }
    });
  }

}
