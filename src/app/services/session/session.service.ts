import { Injectable } from '@angular/core';
import { StorageService } from '../storage.service';

@Injectable()
export class SessionService {

  constructor(private storage: StorageService) { }

  isLoggedIn() {
    const value = this.storage.get('isLoggedIn');
    if (value === 'true') {
      return true;
    } else {
      return false;
    }
  }

  getToken() {
    return this.storage.get('token');
  }

  getUser() {
    return this.storage.get('user');
  }

  setSession() {
    this.storage.set('isLoggedIn', 'true');
    return true;
  }

  setToken(token) {
    this.storage.set('token', token);
    return true;
  }

  setUserDetails(user) {
    this.storage.set('user', JSON.stringify(user));
    return true;
  }

  clearSession() {
    this.storage.flush();
  }

  getUserId() {
    const user = JSON.parse(this.getUser());
    return user['userid'];
  }

}
