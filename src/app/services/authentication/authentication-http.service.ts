import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppSettings } from '../../app.settings';

@Injectable()
export class AuthenticationHttpService {

  constructor(private http: HttpClient) { }

  /**
   *
   * @param data
   */
  login(data): Observable<any> {
    return Observable.create(observer => {
      try {
        this.http.post(AppSettings.API_URL + 'userLogin/', data)
        .subscribe(result => {
          if (result['status'] === 200) {
            observer.next({status: 'OK', data: result['details']});
            observer.complete();
          } else {
            observer.next({status: 'ERROR', message: result['message'], details: result});
            observer.complete();
          }
        },
        error => {
          observer.error({status: 'ERROR', message: 'login http requset error', details: error});
          observer.complete();
        });
      //   let result = {
      //     "status": 200,
      //     "success": true,
      //     "message": "Login Successful",
      //     "details": {
      //         "0": {
      //             "userid": "210",
      //             "username": "vinu",
      //             "email": "vinusurendran1988@gmail.com",
      //             "usertype": "1"
      //         },
      //         "token": "6be04d7da8cb6eb6afd8db2113b1ad85"
      //     }
      // }
      //   setTimeout(function () {
      //     observer.next({status: "OK", data: result.details})
      //     observer.complete();
      //   }, 1000);
      } catch (error) {
        observer.error({status: 'ERROR', message: 'login syntax error', details: error});
        observer.complete();
      }
    }); // end of Observer
  }

  /**
   *
   */
  register(data): Observable<any> {
    return Observable.create(observer => {
      try {
        this.http.post(AppSettings.API_URL + 'signupInfo/', data)
        .subscribe(result => {
          if (result['status'] === 201) {
            observer.next({status: 'OK', data: result});
            observer.complete();
          } else {
            observer.error({status: 'ERROR', message: result['message'], details: result});
            observer.complete();
          }
        },
        error => {
          observer.error({status: 'ERROR', message: 'registration http requset error', details: error});
          observer.complete();
        });
      } catch (error) {
        observer.error({status: 'ERROR', message: 'registration syntax error', details: error});
        observer.complete();
      }
    });
  }

}
