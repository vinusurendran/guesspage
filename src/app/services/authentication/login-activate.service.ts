import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

// app
import { AuthenticationService } from './authentication.service';
import { PublisherService } from '../publisher.service';
import { SessionService } from '../session/session.service';

@Injectable()
export class LoginActivate implements CanActivate {

    constructor(
        private authService: AuthenticationService,
        private router: Router,
        private publish: PublisherService,
        private session: SessionService
    ) { }
    canActivate(
        route: ActivatedRouteSnapshot,
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (!this.session.isLoggedIn()) {
            this.router.navigate(['/home']);
            setTimeout(() => {
                this.publish.broadcast('event.authentication.notLoggedIn', { title: 'success', message: 'Please login to continue.' });
            }, 500);
        }
        return true;
    }

}
