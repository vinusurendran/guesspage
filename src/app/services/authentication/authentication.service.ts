import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AuthenticationHttpService } from './authentication-http.service';
import { SessionService } from '../session/session.service';

@Injectable()
export class AuthenticationService {

  constructor(
    private authHttp: AuthenticationHttpService,
    private session: SessionService
  ) { }

  /**
   *
   * @param data
   */
  authenticate(data) {
    return Observable.create(observer => {
      try {
        this.authHttp.login(data)
        .subscribe(result => {
          if (result.status === 'OK') {
            this.session.setSession();
            this.session.setToken(result.data.token);
            this.session.setUserDetails(result.data['0']);
            observer.next({status: 'OK'});
            observer.complete();
          } else {
            observer.next({status: 'ERROR', message: 'Invalid credentials.'});
            observer.complete();
          }
        },
        error => {
          console.log(error);
          observer.error({status: 'ERROR', message: 'Something went wrong.'});
          observer.complete();
        });
      } catch (error) {
        console.log(error);
        observer.error({status: 'ERROR', message: 'Something went wrong.'});
        observer.complete();
      }
    });
  }

  /**
   *
   */
  register(object) {
    return Observable.create(observer => {
      try {
        this.authHttp.register(object)
        .subscribe(result => {
          observer.next({status: 'OK'});
          observer.complete();
        },
        error => {
          console.log(error);
          observer.error({status: 'ERROR', message: error.message});
          observer.complete();
        });
      } catch (error) {
        console.log(error);
        observer.error({status: 'ERROR', message: 'Something went wrong.'});
        observer.complete();
      }
    });
  }

  /**
   *
   */
  logout() {
    this.session.clearSession();
  }
}
