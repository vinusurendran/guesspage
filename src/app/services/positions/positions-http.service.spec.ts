import { TestBed, inject } from '@angular/core/testing';

import { PositionsHttpService } from './positions-http.service';

describe('PositionsHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PositionsHttpService]
    });
  });

  it('should be created', inject([PositionsHttpService], (service: PositionsHttpService) => {
    expect(service).toBeTruthy();
  }));
});
