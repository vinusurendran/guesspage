import { TestBed, inject } from '@angular/core/testing';

import { EmployerHttpService } from './employer-http.service';

describe('EmployerHttpService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployerHttpService]
    });
  });

  it('should be created', inject([EmployerHttpService], (service: EmployerHttpService) => {
    expect(service).toBeTruthy();
  }));
});
