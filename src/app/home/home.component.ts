import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { map } from 'rxjs/operators';
import { AppSettings } from '../app.settings';
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public success: any = false;
  public error: any = false;
  constructor(private route: ActivatedRoute) {
    console.log(AppSettings.API_URL);
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params['status'] == 'activation-success') {
        this.success = true;
        $("#myModal-signup-success").modal('show');
      } else if (params['status'] == 'activation-error') {
        this.error = true;
        $("#myModal-signup-success").modal('show');
      }
    });
    
  }

}
