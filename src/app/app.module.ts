import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  ReactiveFormsModule,
  FormsModule,
} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PublisherService } from './services/publisher.service';
import { SessionService } from './services/session/session.service';
import { StorageService } from './services/storage.service';
import { UserService } from './services/user/user.service';
import { UserHttpService } from './services/user/user-http.service';
import { LoginActivate } from './services/authentication/login-activate.service';
import { AuthenticationService } from './services/authentication/authentication.service';
import { AuthenticationHttpService } from './services/authentication/authentication-http.service';
import { LookupService } from './services/shared/lookup.service';
import { LookupHttpService } from './services/shared/lookup-http.service';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { ROUTING } from './app.routing';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { PositionsComponent } from './positions/positions.component';
import { EmployerComponent } from './employer/employer.component';
import { AgencyComponent } from './agency/agency.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { RegLoginComponent } from './shared/reg-login/reg-login.component';
import { ProfileComponent } from './user/profile/profile.component';
import { CandidatesComponent } from './user/candidates/candidates.component';
import { CandidatesListComponent } from './user/candidates/list.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    PositionsComponent,
    EmployerComponent,
    AgencyComponent,
    HeaderComponent,
    FooterComponent,
    RegLoginComponent,
    ProfileComponent,
    CandidatesComponent,
    CandidatesListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ROUTING,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    })
  ],
  providers: [
    PublisherService,
    SessionService,
    StorageService,
    LoginActivate,
    AuthenticationService,
    AuthenticationHttpService,
    LookupHttpService,
    LookupService,
    UserService,
    UserHttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
