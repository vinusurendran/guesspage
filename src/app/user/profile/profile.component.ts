import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { LookupService } from '../../services/shared/lookup.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'guesspage-user-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public profileData = {};
  public profileDataError = {
    flag: false,
    message: 'Couldn\'t fetch profile data.'
  };
  public countryList  = [];
  public stateList    = [{province_id: "", province_name: "Select State"}];
  public locationList = [{location_id: "", location_name: "Select Location"}];
  public geoLoc       = {latitude: 0, longitude: 0};
  constructor(
    private _user: UserService,
    private _lookup: LookupService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    if (window.navigator && window.navigator.geolocation) {
      window.navigator.geolocation.getCurrentPosition(
        position => {
          // this.geolocationPosition = position,
          this.geoLoc['latitude'] = position.coords.latitude
          this.geoLoc['longitude'] = position.coords.longitude
          console.log(position)
        },
        error => {
          switch (error.code) {
            case 1:
              console.log('Permission Denied');
              break;
            case 2:
              console.log('Position Unavailable');
              break;
            case 3:
              console.log('Timeout');
              break;
          }
        }
      );
    };
  }

  getProfile() {
    this._user.getProfile()
    .subscribe(
      result => {
        this.profileDataError.flag = false;

        // Loading default if no value
        result.data.countryid = "";
        result.data.stateid = "";
        result.data.locationid = "";

        if ('CountryID' in result.data && result.data.CountryID) {
          result.data.countryid = result.data.CountryID;
          delete result.data.CountryID;
          this.getStateList(result.data.countryid);
        }
        if ('StateID' in result.data && result.data.StateID) {
          result.data.stateid = result.data.StateID;
          delete result.data.StateID;
          this.getLocationList(result.data.stateid);
        }
        if ('LocationID' in result.data && result.data.LocationID) {
          result.data.locationid = result.data.LocationID;
          delete result.data.LocationID;
        }
        this.profileData = result.data;
        this.toastr.success('Profile data fetched.', 'Success', );
      },
      error => {
        if (error['status'] == "204") {
          this.profileDataError.flag = true;
        }
        
        this.toastr.error(error.status, error.message);
      }
    );
  }

  updateProfile() {
    this.profileData['latitude'] = this.geoLoc['latitude']
    this.profileData['longitude'] = this.geoLoc['longitude']
    this.profileData["profileImage"] = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/4RTfRX";
    this.profileData['extension'] = "+91";
    this._user.updateProfile(this.profileData)
    .subscribe(
      result => {
        this.toastr.success('Profile data updated.', 'Success', );
      },
      error => {
        this.toastr.error(error.status, error.message);
      }
    );
  }

  getCountryList() {
    this._lookup.getCountryList()
    .subscribe(
      result => {
        this.countryList = [{country_code: "", country_name: "Select Country"}].concat(result.data);
      },
      error => {
        this.toastr.error(error.status, error.message);
      }
    );
  }

  getStateList(countryId) {
    if (!countryId || countryId == "undefined") {
      this.stateList = [{province_id: "", province_name: "Select State"}];
      return;
    }
    this._lookup.getStateList(countryId)
    .subscribe(
      result => {
        this.stateList = [{province_id: "", province_name: "Select State"}].concat(result.data);
      },
      error => {
        this.toastr.error(error.status, error.message);
      }
    );
  }

  getLocationList(stateId) {
    if (!stateId || stateId == "undefined") {
      this.locationList = [{location_id: "", location_name: "Select Location"}];
      return;
    }
    this._lookup.getLocationList(stateId)
    .subscribe(
      result => {
        this.locationList = [{location_id: "", location_name: "Select Location"}].concat(result.data);
      },
      error => {
        this.toastr.error(error.status, error.message);
      }
    );
  }

}
