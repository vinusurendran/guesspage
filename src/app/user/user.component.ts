import { Component, OnInit, ViewChild } from '@angular/core';

import { UserService } from '../services/user/user.service';
import { ProfileComponent } from './profile/profile.component';
import { CandidatesListComponent } from './candidates/list.component';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @ViewChild(ProfileComponent ) profile;
  @ViewChild(CandidatesListComponent ) candidateList;

  constructor(
    private _user: UserService
  ) { }

  ngOnInit() {}

  getProfile() {
    this.profile.getProfile();
    this.profile.getCountryList();
  }

  getCandidateList() {
    this.candidateList.getCandidateList();
  }

}
