import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { UserService } from '../../services/user/user.service';
import { ToastrService } from 'ngx-toastr';
import { PublisherService, PublishData } from '../../services/publisher.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'guesspage-user-candidate-modal',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.css']
})
export class CandidatesComponent implements OnInit {
  public candidate:any = {};
  constructor(
    private _userService: UserService,
    private toastr: ToastrService,
    private _publisher: PublisherService
  ) { }

  ngOnInit() { }

  addCandidate() {
    let pipe = new DatePipe('en-US');

    let date = new Date(this.candidate['dob']);
    this.candidate['dob'] = pipe.transform(date, 'dd-MM-yyyy');
    this.candidate['last_working_day'] = pipe.transform(new Date(this.candidate['last_working_day']), 'dd-MM-yyyy');
    this.candidate['candidatephoto'] = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAgEASABIAAD/4RTfRX";
    this.candidate["countryid"] = 12;
    this.candidate["stateid"] = 136;
    this.candidate["currentlocation"] = 1;
    this.candidate["preferedlocation"] = 1;

    this._userService.addCandidate(this.candidate)
    .subscribe(result => {
      this.toastr.success('Candidate added.', 'Success');
      this._publisher.broadcast('candidate.add', {});
    }, error => {
      this.toastr.error(error.status, error.message);
      this._publisher.broadcast('candidate.add', {});
    });
  }

}
