import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { ToastrService } from 'ngx-toastr';
import { PublisherService, PublishData } from '../../services/publisher.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'guesspage-user-candidate-list',
  templateUrl: './list.component.html',
  styleUrls: ['./candidates.component.css']
})
export class CandidatesListComponent implements OnInit {

  public candidateList = [];

  constructor(
    private _userService: UserService,
    private toastr: ToastrService,
    private _publisher: PublisherService
  ) { }

  ngOnInit() {
    this.getCandidateList();
    this._publisher.on<PublishData>('candidate.add')
    .subscribe(data => {
      this.getCandidateList();
    });
  }

  getCandidateList() {
    this.candidateList = [];
    this._userService.getCandidates()
    .subscribe(result => {
      this.candidateList = result.data;
      this.toastr.success('Showing candidate list', 'Success', );
    }, error => {
      this.toastr.error(error.status, error.message);
    });
  }

}
